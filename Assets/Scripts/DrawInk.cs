using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ClientDll;

public class InkStroke
{
	public List<Vector3> points;
		
	public InkStroke ()
	{
		points = new List<Vector3> ();
	}
}

public class DrawInk : MonoBehaviour
{

	public Client client;
	public Material mat;
	Vector3 currMousePos = Vector3.zero;
	int canvasWidth = 700;
	int canvasHeight = 200;
	Rect canvasRect;
	List<InkStroke> strokes = new List<InkStroke> ();
	bool buttonPressed = false;
	bool drawing = false;
	Vector3 screenPos = Vector3.zero;
	InkStroke prevStrokeUnfilt;
	
	// Use this for initialization
	void Start ()
	{
		canvasRect = new Rect (Screen.width - canvasWidth, Screen.height - canvasHeight, canvasWidth, canvasHeight);
		//GameObject.Find("ZSCore").GetComponent<ZSCore>().SetMouseEmulationEnabled(true);
		
		client = new Client();
		client.Connect();
		if (!client.Initialized)
			Debug.Log("Failed to connect to recognition engine");
	}
	
	// Update is called once per frame
	void Update ()
	{
		currMousePos = Input.mousePosition;

		// Check if button pressed
		buttonPressed = Input.GetMouseButton (0);
		//buttonPressed = GameObject.Find("ZSStylus").GetComponent<ZSStylusSelector>().GetButton(0); //screenPos.z > 0.39 ? true : false; //
		
		if (buttonPressed && !drawing) {
			drawing = true;
			InkStroke newStroke = new InkStroke ();
			strokes.Add (newStroke);
			prevStrokeUnfilt = new InkStroke ();
		} 
		else if (drawing && !buttonPressed) 
		{
			drawing = false;
			//Do Gaussian Smoothing of entire list.
			if (strokes.Count > 0)
			{
				InkStroke filteredStroke = dehooking( FilterStroke( strokes[strokes.Count-1] ) );
				filteredStroke = gaussFilterStroke( filteredStroke );
				strokes [strokes.Count - 1] = filteredStroke; //prevStrokeUnfilt;//
				StrokeCollected( strokes [strokes.Count - 1] );
				//prevStrokeUnfilt = null;
			}
		}
		
		if( drawing )
		{
			Vector3 clickLocation = new Vector3 (currMousePos.x, currMousePos.y, 0); //new Vector3(screenPos.x + 5, screenPos.y + 100, 0); //
			//print("Drawing"+ Time.deltaTime);
			// Check if mouse is within the bounds of the canvas, X < 1920, X > 1200, Y < 200, Y > 0
			//if (drawing) //&& currMousePos.x > canvasRect.x && currMousePos.x < canvasRect.x + canvasRect.width && currMousePos.y < canvasRect.height && currMousePos.y > 0) 
			prevStrokeUnfilt.points.Add (clickLocation);
			if( strokes.Count > 0 )
				strokes [strokes.Count - 1].points.Add (clickLocation);//gaussFilterSingle( prevStrokeUnfilt, prevStrokeUnfilt.points.Count-1 ));//clickLocation);//
		}
		//else			print("Not"+ Time.deltaTime);
	}
	
	void StrokeCollected (InkStroke stroke)
	{
		string strokeString = Messages.InkStroke + ",";
        for(int i=0 ; i< stroke.points.Count ; ++i)
        {
            Vector3 pt = stroke.points[i];
            strokeString += "," + pt.x.ToString() + "," + (Screen.height - pt.y).ToString();
        }
		
        client.Send(strokeString);
        string result = client.Receive();
		
		Debug.Log(ExtractMath(result));
	}
	
	string ExtractMath (string serverMsg)
	{
		string[] values = serverMsg.Split (new char[] { ',' });

		//Skip "[message_type],," in the front of the response
		if (values.Length >= 3)
			return values [2];
		else
			return "";
	}
	
	void OnPostRender ()
	{
		if (!mat) {
			Debug.LogError ("Please Assign a material on the inspector");
			return;
		}
		
		// Start Drawing GL lines
		GL.PushMatrix ();
		mat.SetPass (0);
		//GL.LoadOrtho();
		GL.LoadPixelMatrix ();
		
		GL.Begin (GL.QUADS);
		GL.Color (Color.white);
		/*
		 * GL.Vertex3(1200,0,0);
    	GL.Vertex3(1200,200,0);
    	GL.Vertex3(1920,200,0);
    	GL.Vertex3(1920,0,0);
    	*/
		GL.Vertex3 (canvasRect.x, Screen.height - canvasRect.y, 0);
		GL.Vertex3 (canvasRect.x, Screen.height - (canvasRect.y + canvasRect.height), 0);
		GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - (canvasRect.y + canvasRect.height), 0);
		GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - canvasRect.y, 0);
		GL.End ();
		
		mat.SetPass (1);
		GL.Begin (GL.LINES);
		GL.Color (Color.black);
		
		// Draw ink to canvas 	
		foreach (InkStroke stroke in strokes) {
			int i = 0;
			while (i < stroke.points.Count-1) {
				GL.Vertex (stroke.points [i]);// + new Vector3(20,0,0));
				GL.Vertex (stroke.points [i + 1]);// +  new Vector3(20,0,0));
				i++;
			}	
		}
		GL.End ();
		
		// Draw current stroke unfiltered to canvas 
		/*if (prevStrokeUnfilt != null) {   
			mat.SetPass (0);
			GL.Begin (GL.LINES);
			GL.Color (Color.white);
		

			for (int i=0; i < prevStrokeUnfilt.points.Count-1; i++) {
				GL.Vertex (prevStrokeUnfilt.points [i]);
				GL.Vertex (prevStrokeUnfilt.points [i + 1]);
			}	

			// Stop Drawing
			GL.End ();

		}*/
		GL.PopMatrix ();  
	}
	
	void OnGUI ()
	{
		GUI.Label (new Rect (10, 170, 400, 20), "Mouse X:  " + currMousePos.x.ToString ());
		GUI.Label (new Rect (10, 210, 400, 20), "Mouse Y:  " + currMousePos.y.ToString ());							
		
		//GUI.Label(new Rect(10, 250, 400, 20), "Stylus Mouse X:  " + screenPos.x.ToString());
		//GUI.Label(new Rect(10, 290, 400, 20), "Stylus Mouse Y:  " + screenPos.y.ToString());
		//GUI.Label(new Rect(10, 330, 400, 20), "Stylus Mouse Z:  " + screenPos.z.ToString());

		GUI.Label (new Rect (10, 370, 400, 20), "Button pressed:  " + buttonPressed.ToString ());	
		//GUI.Label(new Rect(10, 410, 400, 20), "Mouse enabled:  " +  GameObject.Find("ZSCore").GetComponent<ZSCore>().IsMouseEmulationEnabled());
		if (GUI.Button(new Rect(10,70,100,30),"Clear Strokes"))
			strokes.Clear();
			
	}
	
	

	/** See Gaussian Smoothing Slide of
	 *  http://www.eecs.ucf.edu/courses/cap6105/fall2012/lectures/preprocessing.pdf
	 *  for explanation of filtering algorithm and nomenclature.
	 **/
	public int gaussSigma = 5;

	private Vector3 gaussFilterSingle (InkStroke ink, int i)
	{
		int threeSigma = 3 * gaussSigma;
		int rangeMin = i - threeSigma;
		int rangeMax = i + threeSigma;
		if (rangeMin < 0 || rangeMax >= ink.points.Count) {
			return ink.points [i];
		}
		
		float twoSigmaSqrd = 2 * Mathf.Pow (gaussSigma, 2);
		
		float w_j_denom = 0f;
		for (int k = -threeSigma; k <= threeSigma; k++) {
			w_j_denom += Mathf.Exp (-1 * (Mathf.Pow ((float)k, 2f)) / twoSigmaSqrd);
		}


		List<Vector3> subsetPoints = new List<Vector3> ();
		List<Vector3> subsetPointsTail = new List<Vector3> ();
		/*while( rangeMin < 0 )
		{
			subsetPoints.Add( ink.points[0] );
			rangeMin++;
		}

		while( rangeMax > ink.points.Count )
		{
			subsetPointsTail.Add( ink.points[ink.points.Count-1] );
			rangeMax--;
		}
		*/
		subsetPoints.AddRange (ink.points.GetRange (rangeMin, 1+rangeMax - rangeMin));
		//subsetPoints.AddRange( subsetPointsTail );

		int j = -threeSigma;
		Vector3 filteredPoint = new Vector3 (0, 0, 0);
		for (j = -threeSigma; j <= threeSigma; j++) {
			float w_j = Mathf.Exp (-1 * Mathf.Pow ((float)j, 2f) / twoSigmaSqrd) / w_j_denom;
			//subsetPoints[j+threeSigma].Scale(new Vector3(w_j, w_j, w_j));
			filteredPoint = filteredPoint + (subsetPoints [j + threeSigma] * w_j);
		}
		
		// Is this correct - there was no return val
		return filteredPoint;
	}

	private InkStroke gaussFilterStroke (InkStroke ink)
	{
		InkStroke filtered = new InkStroke ();
		int i = 0;
		for (i = 0; i < ink.points.Count; i++) {
			Vector3 point = gaussFilterSingle (ink, i);
			if( point != null )
				filtered.points.Add (point);
		}
		return filtered;
	}
	
	/** See Gaussian Smoothing Slide of
	 *  http://www.eecs.ucf.edu/courses/cap6105/fall2012/lectures/preprocessing.pdf
	 *  for explanation of filtering algorithm and nomenclature.
	 **/
	
	public float hookMinPercent = 50;
	public float hookMaxPercent = 50;
	public float dehookDistanceSqr = 400f;	
	
	private InkStroke dehooking(InkStroke ink)
	{
		int hookMin = (int)(ink.points.Count*hookMinPercent/100);
		int hookMax = (int)(ink.points.Count*hookMaxPercent/100);
		print( "Count: " + ink.points.Count + " Hookmin " + hookMin + " hookMax " + hookMax );
		float maxDistSqr = 0f;
		List<Vector3> badPtsToRemove = new List<Vector3>();
		float distSqr = 0;
		for( int i=1; i< Mathf.Min(hookMin, ink.points.Count - hookMax); i++ )
		{
			distSqr = (ink.points[i]-ink.points[0]).sqrMagnitude;
			print( "i: " + i + "dist|max: " + distSqr + " " + maxDistSqr );
			if( distSqr > dehookDistanceSqr )
				break;
			if( distSqr >= maxDistSqr )
			{
				maxDistSqr = distSqr;
			}
			else
			{
				for( int j=0; j<i; j++ )
					badPtsToRemove.Add(ink.points[j]);
				break;
			}
		}
		maxDistSqr = 0;
		for( int i = ink.points.Count-2; i > Mathf.Max(hookMax, ink.points.Count - hookMin); i-- )
		{			
			distSqr = (ink.points[ink.points.Count-1]-ink.points[i]).sqrMagnitude;
			if( distSqr > dehookDistanceSqr )
				break;
			if( distSqr >= maxDistSqr )
			{
				maxDistSqr = distSqr;
			}
			else
			{
				for( int j = ink.points.Count - 1; j > i; j-- )
					badPtsToRemove.Add( ink.points[j] );
				break;
			}
		}
		print( "Hook: " + badPtsToRemove.Count);
		return RemovePointsFromPointsList(badPtsToRemove, ink);
	}
	public float SelfIntersectionThreshold = 5;
	private InkStroke FilterStroke( InkStroke ink )
	{
		Vector3 curPt = ink.points[0];
		List<Vector3> badPtsToRemove = new List<Vector3>();
		
		for( int i=1; i< ink.points.Count; i++ )
		{
			if( curPt == ink.points[i] ) 
				badPtsToRemove.Add( ink.points[i] );
			else 
				curPt = ink.points[i];
		}
		
		return RemovePointsFromPointsList(badPtsToRemove, ink);			
	}
	
	private InkStroke RemovePointsFromPointsList( List<Vector3> badPtsToRemove, InkStroke ink )
	{
		while( badPtsToRemove.Count > 0 )
		{
			ink.points.Remove( badPtsToRemove[badPtsToRemove.Count-1] );
			badPtsToRemove.RemoveAt(badPtsToRemove.Count-1);
		}
		return ink;
	}
}
