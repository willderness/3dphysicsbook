using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class ZSRightCamera : MonoBehaviour
{
  void Start()
  {
    GameObject coreObject = GameObject.Find("ZSCore");

    if (coreObject != null)
      _core = coreObject.GetComponent<ZSCore>();
  }

  void OnPreCull()
  {
    if (_core != null)
    {
      // Specify the correct eye.
      ZSCore.Eye eye = ZSCore.Eye.Right;

      if (_core.AreEyesSwapped())
        eye = ZSCore.Eye.Left;

      if (_core.m_currentCamera != null)
      {
        // Grab the current monoscopic camera's transform.
        _core.transform.position   = _core.m_currentCamera.transform.position;
        _core.transform.rotation   = _core.m_currentCamera.transform.rotation;
        _core.transform.localScale = _core.m_currentCamera.transform.localScale;
      }

      // Calculate right camera's transform.
      Matrix4x4 localToParent = Matrix4x4.Scale(new Vector3(1, 1, -1)) * _core.GetViewMatrix(eye).inverse;
      Vector4 localPosition   = localToParent * new Vector4(0, 0, 0, 1);
      Vector4 localForward    = localToParent * new Vector4(0, 0, -1, 0);
      Vector4 localUp         = localToParent * new Vector4(0, 1, 0, 0);

      transform.localPosition = localPosition;
      transform.localRotation = Quaternion.LookRotation(localForward, localUp);
      
      gameObject.camera.projectionMatrix = _core.GetProjectionMatrix(eye);
    }

    GL.IssuePluginEvent((int)ZSCore.GlPluginEventType.SelectRightEye);
  }

  private ZSCore _core = null;
}
